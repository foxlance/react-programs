import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Programs from './components/Programs'
import Program from './components/Program'
import ProgramForm from './components/ProgramForm'
import ProgramUpdate from './components/ProgramUpdate'
import './App.css';

const client = new ApolloClient({
  // uri: 'https://stark-tor-50435.herokuapp.com/graphiql',
  uri: 'http://localhost:5000/graphql',
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <Route exact path="/" component={Programs} />
          <Route exact path="/programs/:id(\d+)" component={Program} />
          <Route exact path="/programs/:id(\d+)/edit" component={ProgramUpdate} />
          <Route exact path="/programs/create" component={ProgramForm} />
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
