import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const createProgram = gql`
  mutation createProgram(
    $operation: String!,
    $processId: String!,
    $processStep: Int!,
    $productFamily: String!,
  ) {
    createProgram(
      operation: $operation,
      processId: $processId,
      processStep: $processStep,
      productFamily: $productFamily,
    ) {
      id
    }
  }
`;

class ProgramForm extends Component {

  onSubmit = (e) => {
    e.preventDefault();

    this.props.createProgram({
      variables: {
        operation: this.operation.value,
        processId: this.processId.value,
        processStep: parseInt(this.processStep.value),
        productFamily: this.productFamily.value,
      }
    }).then(res => {
      window.location = `/programs/${res.data.createProgram.id}`
    }).catch((e) => {
      alert(e.message);
    });
  }

  render() {
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-lg-16">
            <h1 className="display-3">Create Program</h1>

            <form onSubmit={this.onSubmit}>
              <fieldset>
                <div className="form-group">
                  <label htmlFor="operation">Operation</label>
                  <input type="text"
                         className="form-control"
                         name="operation"
                         placeholder="e.g. SMD Top side"
                         required
                         ref={input => (this.operation = input)}
                  />
                </div>
              </fieldset>

              <fieldset>
                <div className="form-group">
                  <label htmlFor="operation">Process ID</label>
                  <input type="text"
                         className="form-control"
                         name="processId"
                         placeholder="e.g. 13002"
                         required
                         ref={input => (this.processId = input)}
                  />
                </div>
              </fieldset>

              <fieldset>
                <div className="form-group">
                  <label htmlFor="operation">Process Step</label>
                  <input type="number"
                         className="form-control"
                         name="processStep"
                         placeholder="e.g. 25"
                         required
                         ref={input => (this.processStep = input)}
                  />
                </div>
              </fieldset>

              <fieldset>
                <div className="form-group">
                  <label htmlFor="operation">Product Family</label>
                  <input type="text"
                         className="form-control"
                         name="productFamily"
                         placeholder="e.g. Nissan Juke PCBA"
                         required
                         ref={input => (this.productFamily = input)}
                  />
                </div>
              </fieldset>

              <button type="submit" value="submit" className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default graphql(createProgram, {
  name: 'createProgram'
})(ProgramForm)
