import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const deleteProgram = gql`
  mutation deleteProgram($id: Int!) {
    deleteProgram(id: $id) {
      id
    }
  }
`;

class ProgramItem extends Component {

  deleteProgram = (id) => {
    this.props.deleteProgram({
      variables: { id }
    }).then(res => {
      window.location = '/';
    }).catch((e) => {
      alert(e.message);
    });
  }

  render() {
    const {
      program: {
        id,
        operation,
        processId,
        processStep,
        productFamily
      }
    } = this.props;

    return (
      <tr>
        <td>{ id }</td>
        <td>{ operation }</td>
        <td>{ processId }</td>
        <td>{ processStep }</td>
        <td>{ productFamily }</td>
        <td className="text-right">
          <Link to={`/programs/${id}`}>
            <i className="fas fa-search mr-3"></i>
          </Link>
          <Link to={`/programs/${id}/edit`}>
            <i className="fas fa-edit mr-3"></i>
          </Link>
          <a href="#" onClick={ this.deleteProgram.bind(this, id) }>
            <i className="fas fa-trash-alt mr-3"></i>
          </a>
        </td>
      </tr>
    )
  }
}

export default graphql(deleteProgram, {
  name: 'deleteProgram'
})(ProgramItem)
