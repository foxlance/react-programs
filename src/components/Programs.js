import React, { Component } from 'react'
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import ProgramItem from './ProgramItem'
import { Link } from 'react-router-dom'

const PROGRAMS_QUERY = gql`
  query ProgramsQuery {
    programs {
      id
      operation
      processId
      processStep
      productFamily
    }
  }
`;

export class Programs extends Component {
  render() {
    return (
      <div className="container mt-5">
        <div className="page-header" id="banner">
          <div className="row">
            <div className="col-lg-8 col-md-7 col-sm-6">
              <h1>Programs List</h1>
            </div>
          </div>
        </div>

        <div className="row float-right mb-3">
          <p className="col-lg-12">
            <Link to={`programs/create`} className="btn btn-info">
              <i className="fas fa-plus-square mr-1"></i> Create new Program
            </Link>
          </p>
        </div>

        <Query query={PROGRAMS_QUERY}>
          {({ loading, error, data }) => {
            if (loading) return <h4 className="text-center mt-5"><i className="fas fa-spinner fa-spin"></i></h4>;
            if (error) return <h4 className="text-danger">{error.message}</h4>;

            return (
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Operation</th>
                    <th>Process ID</th>
                    <th>Process Step</th>
                    <th>Product Family</th>
                    <th className="text-right mr-3">Options</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    data.programs.map(program => (
                      <ProgramItem key={program.id} program={program} />
                    ))
                  }
                </tbody>
              </table>
            )
          }}
        </Query>
      </div>
    )
  }
}

export default Programs
