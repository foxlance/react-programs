import React, { Component, Fragment } from 'react'
import gql from 'graphql-tag';
import { Query, graphql } from 'react-apollo';

const PROGRAM_QUERY = gql`
  query ProgramQuery($id: Int!) {
    program(id: $id) {
      id
      operation
      processId
      processStep
      productFamily
    }
  }
`;

const updateProgram = gql`
  mutation updateProgram(
    $id: Int!,
    $operation: String!,
    $processId: String!,
    $processStep: Int!,
    $productFamily: String!,
  ) {
    updateProgram(
      id: $id,
      operation: $operation,
      processId: $processId,
      processStep: $processStep,
      productFamily: $productFamily,
    ) {
      id
    }
  }
`;

class ProgramForm extends Component {

  onSubmit = (e) => {
    e.preventDefault();

    this.props.updateProgram({
      variables: {
        id: parseInt(this.props.match.params.id),
        operation: this.operation.value,
        processId: this.processId.value,
        processStep: parseInt(this.processStep.value),
        productFamily: this.productFamily.value,
      }
    }).then(res => {
      window.location = `/programs/${res.data.updateProgram.id}`
    }).catch((e) => {
      alert(e.message);
    });
  }

  render() {
    const id = parseInt(this.props.match.params.id);

    return (
      <Fragment>
        <Query query={PROGRAM_QUERY} variables={{ id }}>
          {
            ({loading, error, data}) => {
              if (loading) return <h4 className="text-center mt-5"><i className="fas fa-spinner fa-spin"></i></h4>;
              if (error) return <h4 className="text-danger mt-5 text-center">{error.message}</h4>;

              const {
                program: {
                  id,
                  operation,
                  processId,
                  processStep,
                  productFamily
                }
              } = data;

              return <div className="container mt-5">
                      <div className="row">
                        <div className="col-lg-16">
                          <h1 className="display-3">Update Program</h1>

                          <form onSubmit={this.onSubmit}>
                            <fieldset>
                              <div className="form-group">
                                <label htmlFor="operation">Operation</label>
                                <input type="text"
                                      className="form-control"
                                      name="operation"
                                      placeholder="e.g. SMD Top side"
                                      defaultValue={operation}
                                      required
                                      ref={input => (this.operation = input)}
                                />
                              </div>
                            </fieldset>

                            <fieldset>
                              <div className="form-group">
                                <label htmlFor="operation">Process ID</label>
                                <input type="text"
                                      className="form-control"
                                      name="processId"
                                      placeholder="e.g. 13002"
                                      defaultValue={processId}
                                      required
                                      ref={input => (this.processId = input)}
                                />
                              </div>
                            </fieldset>

                            <fieldset>
                              <div className="form-group">
                                <label htmlFor="operation">Process Step</label>
                                <input type="number"
                                      className="form-control"
                                      name="processStep"
                                      placeholder="e.g. 25"
                                      defaultValue={processStep}
                                      required
                                      ref={input => (this.processStep = input)}
                                />
                              </div>
                            </fieldset>

                            <fieldset>
                              <div className="form-group">
                                <label htmlFor="operation">Product Family</label>
                                <input type="text"
                                      className="form-control"
                                      name="productFamily"
                                      placeholder="e.g. Nissan Juke PCBA"
                                      defaultValue={productFamily}
                                      required
                                      ref={input => (this.productFamily = input)}
                                />
                              </div>
                            </fieldset>

                            <input type="hidden"
                                   className="form-control"
                                   name="id"
                                   defaultValue={id}
                            />
                            <button type="submit" value="submit" className="btn btn-primary">Submit</button>
                          </form>
                        </div>
                      </div>
                    </div>
            }
          }
        </Query>
      </Fragment>
    )
  }
}

export default graphql(updateProgram, {
  name: 'updateProgram'
})(ProgramForm)
