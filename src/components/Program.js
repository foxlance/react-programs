import React, { Component, Fragment } from 'react'
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';

const PROGRAM_QUERY = gql`
  query ProgramQuery($id: Int!) {
    program(id: $id) {
      mission_name
      id
      operation
      processId
      processStep
      productFamily
    }
  }
`;

export class Program extends Component {
  render() {
    const id = parseInt(this.props.match.params.id);

    return (
      <Fragment>
        <Query query={PROGRAM_QUERY} variables={{ id }}>
          {
            ({loading, error, data}) => {
              if (loading) return <h4 className="text-center mt-5"><i className="fas fa-spinner fa-spin"></i></h4>;
              if (error) return <h4 className="text-danger mt-5 text-center">{error.message}</h4>;

              const {
                program: {
                  operation,
                  processId,
                  processStep,
                  productFamily
                }
              } = data;

              return  <div className="container mt-5">
                        <div className="jumbotron mb-0">
                          <h1 className="display-3">{ operation }</h1>
                          <p className="lead">Specifications</p>
                          <p>Process ID: { processId }</p>
                          <p>Process Step: { processStep }</p>
                          <p>Product Family: { productFamily }</p>
                          <hr className="my-4" />
                          <p className="lead">
                          <Link to={`/programs/${id}/edit`} className="btn btn-primary btn-lg mr-1">
                            <i className="fas fa-edit mr-1"></i> Edit
                          </Link>
                          <Link to={`/`} className="btn btn-primary btn-lg">
                            <i className="fas fa-arrow-alt-circle-left mr-1"></i> Back to Programs
                          </Link>
                          </p>
                        </div>
                      </div>
            }
          }
        </Query>
      </Fragment>
    )
  }
}

export default Program
