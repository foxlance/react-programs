This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, run:

### `npm run start`

Note: I was never able to make a proper request to https://stark-tor-50435.herokuapp.com/graphiql. In it's place I needed to write a rudimentary graphql server locally,

https://bitbucket.org/foxlance/graph-server/src/master/

Initially, the listing of records and fetching of specific record was made to the SpaceX Public API, specifically /launches. For implementing the Add/Edit/Delete requirements, I needed to switch to the well-known https://jsonplaceholder.typicode.com/posts to simulate the request life cycle.

